# logsnapshot

# script is meant to be run on CTD only and not other appliance
# do note that this script is unsupported and likely be ignored
# purpose of script is to address our deficiency of getting a log bundle from customer should services not be running

To run the script 

1) start a tmux  (we have seen clarotyOS kicking us out due to length of time it took to gather large files) 
2) Run within the tmux terminal 
chmod +x logsnapshot.sh
./logsnapshot.sh 

Where is the dump ?

1) on non clarotyOS this will be in /tmp
2) on clarotyOS this would be in /opt/claroty-platform/sftp/home

What's different from the standard log snapshot 

- I don't know you can tell me if anything is missing 
- we are expecting the log snapshot to be significantly larger than from running the lkpo command (API method) as i might have included additional unintended data
- postgres is also collected 
- 1 round of htop is gathered by this script 
- smartctl output (if smartctl command is installed) 
- iostat -z 2 5 
- meny.pyc output 
- reference output 

http://10.10.6.55:8000/Claroty_Support/AU-Helidon-AE1/dump-full-2022-09-14--09-55-57_site_AU-Helidon-AE1/
http://10.10.6.55:8000/Claroty_Support/PS-UPWRP-CTD01/dump-full-2022-09-14--09-55-25_site_PS-UPWRP-CTD01/
