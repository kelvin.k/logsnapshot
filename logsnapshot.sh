#!/bin/bash
# script written by Kelvin on 15 Aug and script does not represent what Claroty will collect in a log snapshot
# commands ran here are what I would like to retrieve from a system when box is unable to provide log snapshot due to services not started up
# to run script make sure you do it within tmux terminal since clarotyOS will kick you out 
# to run :
# tmux
# ctl + b " 
# one terminal run ./logsnapshot.sh 

debug=0

sitename=$(jq .site.name /opt/icsranger/lkpo.conf  |sed -e 's/"//g')
fname=dump-full-$(date "+%Y-%m-%d--%H-%M-%S")_site_${sitename}
outdir=/tmp/${fname}
outfile=${fname}.tar.xz

# dumps aha out so we can get output from htop
echo "H4sIAM8mDGMAA+x9DXhTRdb/TVugFGyiglZBvZSCZde0DW2hKQUSaEuKhVZapIoKaT7aSJrU5IaW
D7WYVIihiivr8orusrq6urq7+LEsVi3lQ8B1XRFdF1ddkRVNqCiKYvnMe87cuclk2qL7f/f/f97n
+e/lgbnnN2dmzjlz5syZSUvuLKssT1KpBOVJEqYJSGWPMhDaQPFjvhgLYEXCUPj3CmG0MBjoQQyf
QTAklL20a6VMpXzJ8DcF/hYlyXRRkiGhHE35lFLFlIME9jEklO80CgmlIIixdiirxiWjGtfihPIZ
Kse+pMR2SbRdNm2XTfmVch8VbB+nXwr9W0v7q6V6KWUp5Stl+PGpPixZ8d1go1rZDAnlesq3nmt3
HbQbLPzwR0PLeXS8gexyjOqllMo85Dod9ZMKcp1WrdPh8rVqW4smaScV5HjdOROJTBrKO2vufMKv
2FGkMo8QZB/A+uuSZlz40kNXlWjeT3rj5YLjortkw6NYN1KIz5ugEoU2lSYJsUuoDBOuWDvaPt+s
0X8dUtykz/Mw/L2oH/ydAfCFA+CrB8DzB8CvGABfNQA+fwB8xwD4e4LioYnP3AH4Zw+A3zsAPmwA
fPwA+DUD4BkD4M4B8BcHwJ8eAF8Ofy8ULhO6mxcTWll/pRTfy+E/pfg+DhfAny3ovpOERYuaPQ6X
ZF9kaVwi2FodkrCoompRs0+yCHZ3s80lNNs8HrcH+LyS2bIE2RbZzQ6n4JWsDpdgdSyFN4/kdgr2
Bhs0WrQI+F3uRU63xSw53C4A7MwI0MrtkwQ7DOAV7Ban22sTmsxO4MYqaIoDuZgGQCY09yjioAoo
k0da1GQGSewemw3ghia3i8KLhFmVFTNmLpqYUxB7m5hTGHvPBxzXajL5k0JLXItymcSUSbE6fFTw
hmtViSsjHI4LcKWHKea73DEUe/ma0tJlSCcJ5yh/r1ueD1zracz8KvOKMX8kg+9l8EsYfB+DX8rg
GRQfIsRjGD4igycxeDaDJzN4HoOnMHgRg7P7k4HB2fhsYvAhDF7N4KkMXsfgQxl8MYOzdmtk8GEM
3szgwxm8lcEvYPA2Bk9n8DUMrmbwdQyuYfANDH4hg29icHZ9P8ngFzP4ZgYfweAm/+epptCgcQ2i
YGrvlgaFnyfgrtSdQsITLbwIWKLjRsC/6qsM8IY0SRYiB6PwjEtDGl0kso/QSUija0S6CX3KDjS6
RGQzob9GGl0hsonQR5BGF4isI/QhpHHqI22E/jvSKH6kmdD7kcapjiwm9J+QximOVBN6B9I4tRED
oTuRximN5BH6OaRxKiMioX+DNE5hREPoR5HGqYsIhH4IaZyyyLFzSP8EaQ3Rn9D3IH0h0Z/QdyF9
EdGf0MuRvpjoD7RxgSn4dfCc8XrjfGPt/BrTqs+fBN1NHS9niWBDqymUMi4bZyIYyNoAFbrucDEs
49LQw1kmqDeFnsnKg7Ks/X11IBeMYNoTyGoD/tYiA3lfA+/3wvvW9MkysA6AB2nlBvQf+r4J3p+m
70/C+wv0fTO8b6PvW+F9L33vRj+kfe6F9/0U3wfvq+j7AXgP0feD8P4AfceY9gh9P4a+St974f1Z
+o5B5mX6ngrvr9J3Dby/Ce+lofVZpSrZMBnEAvvVgUdgmveUZpF5hDILS+MrlEIzYi3utILufXjL
owjGbKFMdwJeiyhUQksDLUtpSUIW+r0p+GW4+Ww06u+NGoO71IFhIIPRf2aUOvBQEnm7QB3olbFU
aX9obZYBCP9OVftr6vbrCMfZqDS2VN9z+5Xqq0pxYrueA3Nuw4UDbGEJ+oZa32fGjtKsE/7eAvVq
L9RFs149WGUQ7DnqqwJkPXbdlE9bgWtMh0bqq3D+BTBPCaDt+++40XhiV5tUWxo86D8o+ntVZZcd
Nfo/0Rj9ryaDcCrjy9jY6P8wSuRAvTacIXqpA7eDoMEdXQvB3rrubTLfLlX4ANSX6Q/f+TYwy+YN
287ETNH+Lbi1qWNlltglzDcI4VOQyrV3q9tvgDUZCpBJAzKwGEjTFG3AIgpSKo566HQ0qus2ganQ
0a2m/OHE89XtL8NiNK3aheti4c3Gm4w3G28xLtpJ9exawOoPUkSSQB7GPtcx9Vdh/aFzCfWzmfoo
iBDZkVCve7+s/QRYAqeyoxZ0urooxq/est+0/VCKadUOAyq6HVsHzuEUGITQy1lFqGlUKvOfrrpz
TKw/bp7XQiP/6WrfZ12fMoIsRVu8H/kZM58vZDVjfyfU7asATXCowO8BSWg/ibSH5WKge3Vk7VlZ
LlMI1geY2xQszapGIhvf6vAtD98W41sRvjXimwHfmsN3nyLTK7n9vQvVAYwh/t6bpRn+3mx1+9dg
VJy/espzLawkf2+eUb1+d+RdqLMTv/L3JkmX+ntHSaOQdxLlvcrfO8T3CcDq9qdpN6Owqlulbn8I
gPArw3G5QfvS4CHih4qGtcAWuTRun0qImPdD3W4IcVAslN3avg673HtSHu1Zf+8045oh6sCvk2RP
Nq46E4WdSBrRYXq8wr9H5T85aOlfOkzb/Cf1LbtxuqGZyWTZvzswAjsVpHJ/b5O63TFEjtCIhReC
hCZ06JohKIUVrbujokPKSpHXU2i2xuQ/uUS9en8yLt831lxnEPyyc8js6vbHBmN/UlZ1ZfCz8OuX
y+tj42l5FVXLq+gFGW43Ay9BkPcXlNeNTvQy2REEdeAdZJk3OW6rbNR/JYQ7sHeM6yMQt8vFcCXJ
XNkJXKeRaz3D9UEv4bomgUsFO0/XMwzXFpmrIIHrVexrD8O1TuYqSuDyItdhhutWmas0gWsycmkY
fyjtZU3wR4jT4L0laJxnwRz+MDjU/QBCR6Jafd/d8GqywLs/OkS9+kHcOEPrz6Ajses7/OV38U6l
ibTD9rWDSecG7PyKIXLnAQMKdAXT9nloi1wQcdUBLdSq7zo2mKwD38hQbVZWl4FhvhuYu+oYwAdA
ZAPMCIb/qOxWG3YHUogblsLqLyJ7Hqz9nbhRhOSVXoYiPYe70ukq9X1Pwcs6Pt6M/o7Gm8g9GEYC
JM1Qsob3mKzhOLwT2TemEKk7p0E3L47HLV9d9tbWUUw+EdDjfrie5ASd+ZPjycXDesK9T0kwXtDH
E4zX9fEE4xN9PME4De9bT0yKZxaqyfHMQsExswgzmcUyfTyzuF8fzyye0sczi259PLN4Tx/PLI5R
6TGziDwO5oGVZQpKWRmmYG2WBuNfavhTOCL5YUmrMCZCnowVIWCB+BKal2GC3KP9YghXPSnrKoO9
laFWaLsn/NU30eic9qjv88i15+QMBCeh5w0+Q5Ezj4T0hE9OfmhmEmmS+80j+fdimbiGEDUyUUCI
8ihZUylCRfDYHHXpLohQ49WB53GmI1cyg5DOjcHUyEi5NREikqq0VgfOnuvXXltGxmXiTBb5k2wM
olfkFZkgypJ8WLFA5LFz8awt8qBMkKAZCZ6jw5uC37GyYmWFuvSMP3JjUB2xyE14MxGLR8rPsfqX
of556sBIon8234roL/dWwvVG5jHSezZBIEOiQGUgkBUE2n8WA7qc14fLwECR676FVc/GjDuPk5hR
QtbdTRDU1XcdV5HVp26XgAxhAnIt02AuNIjcCDVds5mIWYTo/ScAvYXhzQS0y84AFyLgZgAB2zWe
4Ho78jWgwZOy8GTLw9MZOLgpZADX7zapS/eot1Snlqq3ZLedUqnvfhV2V/UWk6psydE0o+qNtlNJ
EIQJZkhCrKzDOSjaM1TOC8B5KkMNGeFdX+Fi+cT3JSS9dwyjmUtsj4wMRR0XM1LVolRXo1Q0CA3V
x+OOiDFEiVAYePRM4KlhAk8TE3jGT46fbFTMyWb8D4g/55iTzUgm/uQw8cfExJ96Jv4ocmL8wf4j
Y2AaTBZwLn/vUPXqlahjaP2uPpvTPLBY5BbctMnmJu/eZHOD18hC+KdrGWOwscg+GnbJrg1VcfQC
RHch+jXD+90xQNcg+hCDfoyogOhfGPR1RCch+gKD/gHRU9/JimQRRUZTRfbxigSR95y8y2eB/H/o
xWbwTjbnLJW8OfdpNg+b7UHe0HLww6g0zOS/UzMI9mRgqAjuN76I4WcOxOc5eDYcCx5k9EdUvqPI
BgvqIkywQlUZptAy+Lsgg1nWJXhd0bPY6D+qitxytu8I3wj9jbCdH2Fz3xEMSUwAUW+p0Ki33Kkx
bf841aQ6GXxTvWW2ZvtnqaqTGHIeJUvuhaw2Ei8wYelYT4iuQsYKlV+CFeq+Y1gvZ1l/xLCOQdYH
cYMDmeZg7vAuRLWK0HUaVBmZTB2zeyEUDmnR+4+kKhHMGNRUBv8Uufgk0zAab2hgGy7dEw+apNWH
MD/hjC/gNHK0IvjWLabgIZP/k2PVtRV7uoV89Pvt6yZisceQBkluLjB+Ez974Xll657mxcJ8kxbv
MU3+3nRT8HBL0s07t5Kdpb1b+ubmbVgVPUjPAVu7Gf7tvcnAD/m4afuR6SbVPtNbvT5o3Ck33g+N
TcHdcvtDtH3bVNV8URB8F86HluE7ILzfvHvQF7WioDq+k5yf/FOz5wGDdGVMhBHzt4kuGC14OLrv
5siKKJ5JhIhXLrfloZr+z1PDO45Go2QqwnuPYpWsI6m6+qh8QBlELtzCe6PxeuOCMl3UeD3kCMb5
FcGzxtoy3SfzdUdrdO9Dywzmfgg2WV13z62GwFHf0N0pWcIrONQ2EcfLgO7JEWUb2Y5HZEFWYljT
mFoZbNaY9hgyiBxBjTrwM+LXszKMbSuvwKwQuoFHWlgR3AMC6MCvd6pMq07jxa1UblRvqb1CaDu1
UAp3zN7ddlK11B+AVYD6G9rODmnZ7z+SZ1BveRUbrRmRcm1HyhY4MPtfVXqo7CiY4evEnCAVDvsF
RI3K4EGfDoyQwRz05X2iIrgrYoOut6UWg6Ms/Vyx5Up4C/+xJ3YO7LlQ5m8/IY0whe7M8H+hgpXr
O6br3knwrdhqZ9zHcFlfUdMzLEYHPzKFFmWEC3rQZu/7vlx4fKd9XZy/xhQ83fPjOB0As2K6XJLp
cybcJTyPHYDq6i3JJeMl8Y53S0qkiSXTfOUJNzYdwNVzAPwk4Z6iBdEdRN6E+41FiP+GnisLjeCH
6q7e8GtHAF1P9V9IvJTRD/1rCJEFXMu+Di8ZOxYdA1iz6vSXuMhWPwP/rop+RpT/Vn3fbzDQ4sQH
PwJfC78Ovbd33zGsI6WttCPr1clYcaBTf+8/i4IffXckeKALr2a39161/VTyhB0dKfu3H0yasCf8
0yOY174Wazhcafj0b37zI91r2BDaYIN5qu4JO6W0jpSNCtPwrLLgwRfNJDzswASri4SY4JdggVeO
EE006B9E01Vf4p5r3Ir9QugM9m6PXmX8LmwM7jNu700yTugtDaZAf9t7fif7hXFBRfAkvW0NflsD
dmg0hQadqxYF/zkVrqI89b1PDkF/e4e6P0Q0clEO6xBeNfLr/jJYgEpCjC8ZyouSx5MPB15SUVSk
qIlcSXa5QDmgyAcUxlfIZwDKfFnergx+VxmMzAmlvGPqSPmrQJbF7uNPqAMv4CWUeovGf0SrDhzH
5e0tgDNZWpzjMsjMtj0Enb+YGgfJFe7tyDksBrafgRVoJOLR+5mdKv2Hd64KobFuyDDuMRKJy4LX
Zc0JLsszBsnJp6giaEwlV0XwooHSZAwaR0OpgTIbyhFQFmG+D2UJMmYAo4FcM8ELlsOhhtw7iQDU
wVQ2Vgb3Mqvd2NaaJPhuJeKo771zMO6vK1NSIzdF5fsr0L9a1XaqUd3+E7QdcsMRtT9bXJBC6lVL
1YHvkvuYRXoE6yTpPt4ygU/xVh7qnOqAGrrY9jy2Gx63XBqA8fVl7CRnklfItR/Yse3UdHU7fjAh
i9aOMaurqDi+hjd+BkAjA9yDwGYGWIZAmAEaEcieEgcWIOBjAkM5Am0MRyHpgwGyEBBL4sDFRA4G
SCJyMMBXnwIgTI0DBxGoZoA3EVjDAF0I7GOA3yIgTosDDyPQzAAhBLoZYDkCGdPjgAOBRgaoQ2Ar
A8wikhqY+08ETAwwDoENDDACgb0MkEyGNcaBrw/jsAzwMQJbGWAfAquZadiGgDAjDvwOgWoGeOQw
d1O7FoE1DMeKw3gNHucJ34ocqTPjHDfwfZgQWMxwTOY5xiOwl+EYiYBYGgdS+CbHPwGgleE49AnH
8RYCexmObgTEsjjwe77JzxFoYzg6eI6VCPQyHEsQKCqPAzcisJgBKhDYwABFAOiiYTMU6+gyPHea
LsOev8vxjl/WgfwzkFKsKUhc6z+n8Z+s8fdOxfoLbMfr549Py5HCJZm5ltKfEG9RBzCD3raNC8hj
VXJkqocghfXrC/A6dzcEfRzztwDB1kM+MbWfUu7P5bi4gYabuTG8T+e7Ba5H6blte5DngjhPPY66
ESUeHNe1M4nuAup7H4fXio7KlFTdiU45olZm1ZUGa/ETA31p8KasShMghvAvDkWjsGWa4ARqClXi
Xae6NAS1pSFgrQSkTn33Y+TYN7QzXI4fPkx9uEIUupT1c0jJ4rYcStBT3R7G+5aULKKs+aQSTQPN
AEeeOUeY5fj7b9ik++zPys4c+QJOL52P5hPB/2mKC35pTPCzH8su0Cwt5azsuwAFIQps6pWZvL4h
RA/pMG/8gB83HXlrc/g8VFsXgNsGI+fQWK9XoXCk19nQq13xx9fhlBpZLm87qlZlnLe3/TLBtdYK
dIrvWFERKk1JxXwnI6KCjtpO2X3rZR/roE7wazxGd9SmpHY9RzqpzKpGF6jE+YctvRiv+4rCHxwk
ZwuSOeElXxE6gAEdoBIdoFodeAVNHEpCjqLIT+H42amG/iJP0W220jeM6lCP6X0255WvCjGvxDwm
JnZ2ZDZ29Q7YPHJLXzupA+8gmKB+QNV3AOmxbU9ya/5F6hrEyIdP4KdSI7LCBR9Fo6GisBbVhRMU
OMT+chGyxG7qE6ZQWmcYhAmf/kjxjY/hrTMHdX3lrKyrJabrBBQlcWTpHmTxxFhakWVGAbu6fYXy
7FXEzCBGZoKEnZpZMMqCs30aSG+27/elY7/LFJ94pTK4vXMhSuWCiNc5G9+uYD5nXCCGWlKg1SBw
SWlCqGp0aEj7a74L8MPdrWheo7q0OzgrJTg5WCUGF4xWPofdMysFR+3Cf3pei+dHoUv0l5IzlzQI
D9BCaG6KvirFdzw0N1VflerrCc0drq8a7vtnaK5GX6XxvR+aO0JfNcL3dmhuhr4qw/d6aO5ofdVo
387QXFFfJfpeCpVn6WdlqX/ard6CJzpyHoMkXiwFSRNOwxkwJ1vn15Zri+gSJ0u6rP01ExwI7rjK
uLUTLYWRgoTULg1p9xms7gkfwlSvKIGDYX3X7gJm//8Qk5tZsc/Cw/uQ0Qt5bo0Bjpvz8WOAwE2Q
HXb9jWn19IfoQYe68kxx7GeA9dTQz9sPdRXNYvY/rPov9nwKnX4J+WtXG9PB9bTTMIPN+JA6HmQP
X4EAYR0yhVbkwRFXHbgG5ApP/4rkFicV8cLphKXGhJchF3YZKuK9ffMB6HZtNv64wle4BFcUQVT+
aTIeVMh8Qzc7Jind7EHmmdn44dgzct3bsbqnP8AT24o68Oh1yaTuSuVOKnwftksmt92fyJ8yweaI
l8vIL2lA+Do4594wUxTC37wPxh/H3GbVfICZA2PoGR8Q5b6MjazDkfEMYjXlp9CfJTiQQn6qSVwY
+/mB1yfFu4jCID0vKT+PkVyo9PTZ+zE5U5LoffzxJPwgdna88Q6UsK4wDjyLQDbDsQmBpQzHfQhU
Mxx3vI86+KeoujYrqDrwIowUtiLrs0zbGgTeYIAZCPQwgA6BoYx6IgL7mOHUCIxnOM79HYCZDHAU
gWNMkw8QEK9l7n8RMDFAJwKtDPAUAk8ywAYE9jHAagSEyjiwFIFsBrAh0D2Huf8ngjHATAQy5saB
iQgYGGAMAo0MoPk78ZgNChSOvgfzXFOEH+e9hL6xlbHEP6EuMviskqd0NTKLZQ/UdY1mTL8FgU0M
x+MIFDMc6xHYx3D4sX/hXKz/rczSbsC64fizFOu6IozLVyH+C0xtk5mu9dj1AUbPbARE5tb/Umx3
Jf6czDpw67AKPXv170n/kYVnyL2lKTQzddsLMBb5hKesN7LtNMETPPzNA5jOM97RhcADwBG5jP68
SfjpA5zjPoTAYsayQQSqmcluRaCZARoQWMcA1yNwO9NHGQIPMUDBAc5xxx7gHPciBDYznaoQOMAA
x/6GJy/Gxz5CoIgB/oJAIwO8gsA6BngGgMiM+P7a1czMw31/i8VGEWexXB+v8/6NRG8MgHXTIAAe
fxc/UYnl/V3vMcrOAt6eLeHN7+LPExqDUeP1FUFyX/xXY21lx7jGKaIw39ShxbKmcsJhyIq3n00m
N4OD3sX07ah0pbLfVAaPVAZPlEIf0REfkPtefY/vCF4eMpc0zH3o8Z30Z0f7/qgoeVQJPxUMGzD+
rLXY0uhw2sRms8frcDWIDlezTxLG3+ZzS1OE8eamZvjXia8N8M+4vImtyl9Bq220OZuFUQsLmqqa
8WfBvaMW5jUVpxGkrBWaOm0KpNVKDslpE+a6RfIiNjiW2lxjsMLlhn7MVpsH3uudZssSKL3SMmjb
aLNJ2KKipkq0uK3xRuVmkNgqSm4Rf4RdtKP8meO8mcXiOC/tsrXJqTR1mkGthPZarcXrxWoo5Nbx
GjI01pGXWMV81xKXu8WFZjI32SSbhwyIDZbawHBuF7w5HS6b1u5Ay7S4PVZti8fcDO/NDhfq5PC6
kQeEQaLB5fbYtBYP/Tl7kFNbVFSo145zCCVjSqtm1t5QXSY2Sk3OaWlCCZZEjak4qIJgSYw5TSjJ
lV8AkcWWljXbpmZKtlYpF3TEJjkem9cmifKzwuJ2uj3FOPWSbcrtUOtwgR4SGJWpJbNBavtrG6/t
ry0qPWBlfFiPXJHQMWCkrsFjg8lNrCMYqV1mczrdLYm1bifMFqmtd/psfL+IkUrLMrOLr5RsZiep
JLL1GdW8bECRnI6mgbs13+YzD9htzAwluWTWcPpycS3gS73bukwphXowdYPH7XNZtXJb4lMlzR5s
06dSnhmhvkEbM35fJnnw/nueghI1m13gWFiIFqfZ652aSSkiK1DQxuZBnxcFdDSt1WZxe8gvhRTH
qmAAtxOGt7tdkrbF5mholIoRmSI4JLPTYaE1pMtiGYIm0HJJ304JPEWweNxer82qdfukvjxkCUqN
oFBD4xShEcZz4phogcyY4OO8fdUeO87bjzHGjsub1DpFyJwmjLOATWR7l+Qqc5NLl6HV0YQuIqLJ
ifHJm4fYvUErOw2+UZ+V2cBB8aXZ54E4SV6JA+GL7CxyH7jwBFkaOkw/cuKS6YsSz+wLy1L0p20+
POXl/dTYfZZGr8PcTw1x8L6wvE774vIK7c9lff06IzFOPxVkufY3LJqH2R/Irx6ZwZhNtia3Z5lo
h/0OdzpbhcsLKyN7AtkHRy3UTcnXNRldXodowlhrtJqbIcbj5iVeL8d3MS+nMEeXpvA3mRvNpFoy
L7F5xZpZ82QRYNgK3EFFs8sqkt9s8ooL8mfC/uOCsZtEU+2cSu1M2IywJ5/XViyynZXg1umOb6bT
xIWIaO1kl0LkZmxHogjbLptwkf0Y6ZWEbIy/TsfXCbFfqWGbeiDeeEWp0UbltnvcTaJZ3hXBWGR7
Isq0eMApvXEF0Lzyr3vF+pXDG93Gr6HRTltPdJySn9eERcHkphlYTQafEZtAMgRhm9y0AN2f1Gd6
JcDNHqtIjJuZljgOxiplGFHbTMcpbKoGnOs/jW0n75F12FS7TKwrFmtgkaEJ5AqYFivkQQSR47Bo
c9qabC6JGz+epkBXWm+xON9rA9vFYejFC65qFd12eCWxUq5MS+gHUgNZGiAcKA/04yW5C8kJ6thu
fJJdW5Qj1olNPq8k1ttEXU6OblJif3KKJesnYX+zYNXJk0xSCZgznGqkZdbMukxmDG4++3kyiVtk
ooNgJ+gsLkiL0jg/IJmXrJi2MqYX8TXQbVK+XquTE7M6sjCxApv4zA22hPlSEiu5JyftB520lTQk
OasX5s1BMj2X5HE7Ra/tNp/NZcEB3Wnfo4+lEYa1EQEsPo8Xg4Tb68BlCNv7ErSauzlHrJCuxlEb
3RKOfE3awP05JLEJ9gMXcEImuERscUiN4OHLICK4IUY1sb3STBmS5IH7wwVrszS6iVPfJq4kADYn
wMqEWKB1itPkrmGuE/0ilpUSz2gpFhfAu+h0g9XQxF7F6YmT4KTmiLWNDi/RwUuUSDu/HWfW1OSL
Xl9zs9sj4VzUe9wtEGfBatActh0sm9AOkAJACu2GMXPS4vLFjgJEPlexWOp2XY2LyOL0gZfIdUBC
5GmwuWyw58NCxYB0zffIBSva7nNSX4HOSEAHZQF0kg6ItkQW1l7yOYJGGG2rIg/0JlrdFsyxRagX
62EtEZNl46QzY2Hqk3Y+uWL2If5QUTYhcb7weKLEBQuuH6PVGj+z1GEExumSlzP4JyhktkjirRgZ
zFYI69/n9yUk0/LYnFMz40ErU2z02OxTMSbkTlPGkG2fw8Ut5RRD5gsy2goC4K4rWswejwMWMqQw
ks/j8orZxpqZFRWiFxqJuvxrxJs8ExLjRT8PJEKWRrKUnBiaQBbYb+pBd5etJe6yaPucRDvPhG0N
dn3Ip2KzSxehfG4R46nDNYBZwGKw7cnB0Ex3C7Y/MwQmTwM4n5yuOpbbaLRhVh4EB9yAixPjAtmS
puTnN2WR1ZmwUhv5pVuP8fpqFsIer8Zd6urYyMW6nCJb09WwyhlGsvfjinem/UA7yElm3A5UobhD
4W4zamF+k9Vht+MQ59ULmUStD50Wd+qpZmeLeZkXVwCJIxacMfqWqPM00lSRnLf7vzCPcubSV3wl
TOaw8t/g9omN5qUkicHlLOdcGDutPnkfcMM/HpRa3n0s7iYIXLCk2C6xqff8841Rm0RsbNPPdLPB
2tl3PfSnfz96ShilQT7IbRsgaKOMQMVEFPHIleAXmG3Fz2+YmZolErFJ59je5XHL034NXYUI+pyk
T9hi8VyJOytmvsX9rWPODnOMcxddW1ZWvai8at4cY21txdxZU3XizKrK+XPm1kwtyiPSs8ZZKUIQ
5V0FmLQMohhtprsZgpddkged2GR02lrNqJ04B9RqlG+hRPMSs4jZ8I2O5YSemDcxj9s3Ri0snJI/
qWm5Y7mhCc4Q9e7WHLenQXb+fvgaJanZW5yb2wAe6avPAR/JhdGg/1wqIDUM2UTNzc1wuiXHVBFI
r6/+VhsEaxpfieQFTXOqK3U5OiIe7B8UrJxVXbl04o+pE8ODeYVZXAqnZWvscqw4ni+Oo+kl8Mke
A2Ngmi9fZolmO7m+0tozx1C+kum4jdGLrKmZupy8TBFSJzfkePKVkzh9WlritZRYPX9GZcVM6CY3
F843ubmltaViHXFSaC7WSB6HRcrNLZubKWaimcBKLS0tOS35aNDc2nm5rdiLDpvRV8imsU2OVbLi
fRWRa4xWK2cg8sKH7MFCFoRVDgJoOLC0KB/ccGx6cMsRzzs1EKXICCXyZUtJk00ykxZayBkdS6dm
zoRYC+m+tha2+EySUQJF79NQ2CmYMcIpUpoq38fRR76oA1u6vFP70Vqn1+tlZTMTbvP+hXbyWP+q
3Izr5cIoP27to0OubI/zpQPoBjKbgGei2FpIvM0S+5zJ4xeFQk7sRgPbnY9TfnLYK6x++GMXaPRJ
lKsvf+xS8nvkYPnON35/8p6vX17eHOXiiLWjcsuD49PLoYR6eiei2FPeBn+4PWM3rv3w05tXwhe7
6uyHL3YLy/Qbu4/thz92L8vwx25o+5Wb3tTG78f6n6e4KWL9xm5h++GPXfLG7tgG0o/av79LZnp9
Jtupn2tm5RZtoPlT7tKoHOezc+xq+Xvsy4j0g+zLiBjjP5+dGZF/kJ1jt97fY+c+6yGekgA/f7cb
T1iUJjnkclmxP3vHTDKeuLz0spnhk2+cRXrlHOuPBEDK12d8+QKaXPUzd9B9+RIuoYmfsBfRK2Av
gy2qWL6oMHul7Ml548ZNgLMgMsCRxput0yMSN0wznKlWEGNpvc1mC8gNCDnITxETYG2Te7lWqRPH
OJrwIG52SVPknhJ5B+rDzdYoNwagPey8S8gVQkww9pOEvoFf/jAPnx+ssXD+5+AFBlIe4sqB8P9t
Zff31G/6XyLnDy3beHq4XOaPkMsJtBRouYmWD9Dyt1z92xcn0reOTKT/3eXNIxPLalrOpOUkbvxe
rn0PLTv/TWUPHa/z31Q+8n/Zfv+/lC0j/2cl31/qJf+zcqAnSp/zMp3nGT+2dWJBXt6URFrH0RM5
Op+jCzi6kKMncfRkji7iaD1Hmzm6nqMtHG3laBtH2xNpHae/jtNfx+mv4/TXcfrrOP11nP46Tn8d
p7+O01/H6a/j9Ndx+us4/XWc/rq4/qKQ+Izh6EyOHsvRWRw9jqPHc/TVHJ3N0RM4+kcc/WOOvoaj
tRydw9G5HJ3H0TqOnsjR+RxdwNGFHD2JoydzdBFH6zm6mKP5fIn8oBbzTOXrGxLrp3P1Bo42cvQM
jp7J0aUcXcbR5Rw9i6NNHF3B0bM5+lqOruToORw9l6OrOLqao6/j6HkcXcPRtRw9n6Ov5+gFHF3H
0Tdw9I0cvZCjb+Lomzn6Fo5exNGLOdrM0fUcbeFoK0fbONrO0Q0c3cjRDo6+laOXcLSTo5s42sXR
bo5u5ujbONrD0V6Oljjax9FLObqFo1s5ehlHL+foFRy9kqNv5+g7OJrE/4mJ+1uhJXF/KtTz+z2/
H3/ffpuwX+Xl1edxNJ9f8PlH4v5WyO2Phdx+XcjJU8jtx4X5ifX59WaO5sbj5Mmv59tbufG48Sfy
43P0RI6fy68mTirgaCafaHZMwauKxHpG3mY8jsdDPrG3nstH+PkvKkqcH7OOoydydDz/iQmi1C/D
HwKLP33qSfvJXH8FHK3n/IXzp9vqBxz/X6V/wPzC+Jz+9fkczclfX8jRkzia07+es3/99+if6K9A
Wzial9/G0fZE2sKtTws3/xZOfwunv4XT38Lpb+H0t3D6Wzj9LZz+Fk5/C6e/hdPfwulv4fS3DJz/
knorp7+V09/K6W/l9Ldy+lsT9dcV8v1x+ls5/a2c/lZOf2ui/rrJRd+jHzf/Nm7+bZz+Nk5eG6e/
jdPfxulv4+bfxs2/jdPfxulv4/S3cfrbuPm3cfNv4+bf9j3zb+f0t3P62zn97Zz+dk5/Oz//fHtO
fzunv53T387pb+f0t59Xv4R4rxqVPAW/iwP/w+zNj0WjBxF8HEr8pbc/RKP4S78Ht0Sj5Hee/xiN
4v+eJ0KJ/+1U9dZoFP9DujYon4RyM5R7oVz8YjQahrIZSvz1k+bOaBS/J2Xdq9HoGigPQrkJSsPu
aHQrlNVvRKN5yYLQDWUr/c/qlf8bXbV8nqBq1ahGDR+Suk41RIM4fllGHsjGng3755fPp8i/99Fo
tBp1SNeUp2fMVg9rSW0Tpl8+5Uf5WZlKezzL7IN+ya+pV6ST32LHtphjH1Dw0nTN3LT0VCP9T+nx
/LGS1k/A/meka+5LKkvPuDe5LF3sSClLz147qDQ97+7BM9OL/EPK06uT3ktLLzKm5xnTs2ekizPS
M6DFjPTUWcNievwZ/m4Fm7NnZ5TjE5QPcHLmLk/X+JNu2pmeGq9PUclz9SgCRqxP2qTUY7/46/zN
MHfpVI8KqMTzK/7fY42AnxJk+dcmGdMz7k42pov+lFnpeUkvp6WLgBiJnGXDYnpL0C4P5vyyYQl6
z0C9Z6DexpjepembVMl3qkDz0j6aE1OifLvQfjujUfbMiPgHgBcBzp5t0AbfoO8Brhmi6AvSJucm
paVrjOmppmGKnKPBxzJg/sMaRc5Z/chpRDlnpR8clpyR3O8MkQ7RR5zY35+iUZswwHybsD8T9jcn
3eDsry+i1wu4BqCfxN/Q+s/zn+c/z3+e/zz/J88x+r1v+2ipPCquVL47Zm+qzKd8Z0xKvkxfRmnl
e8RGUVr5TpvLaZlB60dz9d+ei5Krpg1Jcn/Kd+qkDpJp5bt09tJ65XP/NCqg8p03GbRkvzsIn9h3
+tDvk1H2kIO0vZIPKd/No3yXUHiwIQFfQ2lF7sW0VL6jRxn/XFTWx0T5o5RW7HmM0hfR+v9Xj/I9
dvwzeKgsxyW0HE/LSbQsp+X1tLTTcikt76blg7R8gpZbaLmblu/S8lNanqDl4DQ6Pi3H03ISLfEx
N5pzrLZ6H96J3jtufOfZycvrbgQX/PTdF8qThDEq/F4oKffwYwc1Vcl/uVmYfn9ZoHFq8Im/3/Pn
Bw7NfS/F/N7Ku9deVPxnsfaCF4Z/t3HTlkef+NkDYyv9+qf/63fDzpxZv/2267dfcn+u7R9P/PiO
YVsf/0bz3BfHl1z3kLb31OOa/ZfMGKJZsOLrX9UX37BxQtITl/Tc8LOfP/WU96nxBd+mup/5MH/R
ml1l707Q/eromaIVhRtN70z+7baXPgpesezyYWN/dJHqH7+861R561/vLLSfuWe1fdVXlnkP6N2H
XKaZv9x6WLVkc6ou6cbxb44885d5b93158+bGut0u/6xduWkRb9+TXfD3361Ufw875NRt2/8+V8v
6ryu85cZN65aN3bju2Pu+to/9eExm+a3nZl414Pepl8+clPt9JIj+5//fNixNfd4hq+NjrpLfUHl
Av3qM64hOyPjc96ZHOndsaD2F2/eODYQOrP52fe++Mm7oV+8dcvuO5pGnL3ivxcjHTXplEbJd2EJ
jrUrInLZfjBO1t54e/nmBa7nM0yuNL24lfJGRFyDO0b9uDP3tNNS+h1s+rdW36z4wrep9Nm32dLd
jrqTi/9uZllYt3j6/g6nxcJTuK/db5u28J3MDrabmzoWpNVFr3jvpsV1LfKed98TjxuH3i91N7sv
3eO57FJ29BHe2vvViSKezBsOr77ze0I7S8/PVWc1Tk2RvB68yzp5vm9AYMTek+wK3GePmJUz2NS9
mun34Ne0534/eF7tq5j2ha98V/vysmRp3q7dmvISfR/iu4791F/vyGkXKD7Bj8vYiTkrJz3fqFDy
3qopYn211senVrP82Ft1it/ogB/fw0WZCl0eqUe/zk9/tFaAo3jZxqUp9zpy3M9YCH/lbjWLrF0s
mKxwwnHbi0W8Hy7s3yy+rP35t678D1o1HAaFt2ec7DTc03dZdXO0aapq0/opv7dv1rl18MkecfNH
vvs9pko9zDuttEVl5Q29Fy7K8jtfbX8xK+39AwnhScYFWXzvL8kH8Qpfc3Jk+m3WsrX9pfe9RmmO
lbf+JndxX/247+6kG0W/42q+nrvyOpJJL5xrlkLA0sod9138XhbO/3hbRDxTsdbDoOnvlxSnNw/X
dXq4uUb9rCt1ThDszHVKOMszaY9b2yZ7v2A+oyrDwmPO3DMzjy1IrrR9tzxZxHbKlY2ND89pvygp
u5R91uHUa4F79dcazLXuPOO7NTVOri2WfWn7i+TOtucPPm+qF20q1tsepl6+eJGImbPI2kV/X03R
LHghc/9VM6eWjqeL2idBy+IKX++Yo79zcuKeC83dXc2xv3rmK765nstUm56Hvjo/ZYJ+XUQnh3tE
kOu7P1K3U8SszzRnCltevXJux6OnzuU3t5m6/lwpn/U7v5rJ2OL7o0TZP2cWSalI1vdOljnX5hq3
td7k73+/uwsXWH5+rgc6Jn3K6pZnBlzt3AyMr9gvKAD7wec339h4JP03uCxniYxi0CvOAN3El5gE
2lNfklpUwKCXl1+Squfo5KlbkpgO5aXnleollWbmpOhmpjCAeRmJxRkMeimVecWVuRC6pAgiAzu5
AJkTXwTalJ+TCFIIZRXklICszASSoKVuDHppQA5QLj8lsSSRQS81Iz4NdCZCfEZKEYIH0RGfWFSU
WAnRAWNnJReBnZGYm5kMtDq/BExAbIGYmFRcDHZTPLg8BC26Q+KClVAJgOpVUJ0Fqy8R97ZC+OgX
cTKi8UEXhyHftYe4FxXCV0BTz4LGV0TTXwLVXwIVQJ9PR9cP6q9/A9a1MP2w9sUMqABsPhve3kDT
b8cACQOYflj74wRUwAFKg9onjEj6Ye0AUN+cGUk/rD3DAbWQCy3A0MMPNB/7H8n9sPaCEBt29zOh
0aD5z39I+mHtEQ+ofgcG7O6HAdB8IguSebD2TwdUfwKa/ej+L4Hqh82Lw9pTL6D6Ye0v2P2N6Prr
GJDuhGVAtD9PQBXC2pUwgB7/lWj6P0D1f4DqD0ALcAE0ugVNvwO0PeIAbYAeQluQIYDKZehC0w9r
L7NAF0ZwoqlHd/8EBtT8pwDVrwDVr4C24EIBTf8MNP2I+4Uh/Clo6tHtX4am/wZU/w2o/j9ogzHo
6Qc0HgoaP4O1vxH3DWNXz4FG72GA3DcJ0w/rP0gQqf8E1P0w/QpQ/QoE9MPAWQZI3MH0I+6DhvBh
90DD4hemH5YOrqHZD7v39Ec+ce6/jaYf1n+BTcTvIKD/CZr+HVD9O6D60eMbvfx5BTULph92P+se
qH4FNPXo6f89FjOR9aOrR3fPPxz6z8D0o2lAV4sSdkjgDnQhAi+B8hcAIr7OTjB9AAA=" |base64 -d |gunzip > ~/aha

chmod +x ~/aha

cmdtorun=( "df -h" "dmesg -T" "dmidecode" "fdisk -l" "firewall-cmd --direct --get-rules ipv4 filter LKPO_ctd" "firewall-cmd --list-all-zones" "free -m" "hostname" "hostnamectl" "ip a" "iptables -S" "iptables -n -L" "journalctl -k" "journalctl -u auditd" "journalctl -u chronyd" "journalctl -u claroty-installer" "journalctl -u claroty-platform" "journalctl -u icsranger" "journalctl -u icsranger-configurator" "journalctl -u icsranger-watchdog" "journalctl -u jwthenticator" "journalctl -u mariadb" "journalctl -u netunnel" "journalctl -u rabbitmq-server" "journalctl -u redis" "journalctl -u snmpd" "last -n 60" "lkpocli community friends" "lkpocli manager api ranger get_statistics" "lkpocli manager api license get_license_expiry_date" "lm status" "lm sniffers" "lsblk" "lsmod" "lspci" "lspci" "mount" "netstat -i && sleep 2 && netstat -i" "netstat -ntupa" "rabbitmqctl list_queues" "rpm -qa" "systemctl" "timedatectl" "top -b -n1" "top -b -n5" "iostat -z 2 5" "tshark -v" "uname -a" "uptime" "vmstat" "which lsusb && lsusb" ) 

function runcommands {
        IFS=$'\n'
        export IFS
        for mycmd in ${cmdtorun[@]}; do
                        mycmdout=$(echo ${mycmd}|sed -e 's/ /_/g')
                        echo "Processing $mycmdout"
			if [ $debug -gt 0 ] ; then 
	                        echo "eval ${mycmd} 2>&1> $outdir/${mycmdout}" # debugging only 
			fi
                        eval "${mycmd}" > $outdir/${mycmdout}
        done
        unset IFS
        history -w 
	echo "Dumping bash history"
        cat ~root/.bash_history > $outdir/history
        # check on redis status
	echo "Dumping redis status"
        timeout 5 redis-cli -s /tmp/redis.sock monitor > $outdir/redis-cli_-s
        timeout 5 redis-cli -s /tmp/redis.sock --stat >> $outdir/redis-cli_-s
	# dumping drive smart status if available
	echo "Dumping smartctl status"
        for x in $(ls -l /dev/sd* |grep -v [a-z][0-9]|awk '{print $NF}'); do 
        		disk=$(echo ${x}|cut -d'/' -f3)
        		smartctl -A $x > $outdir/smartctl_A_${disk}
        done
	# dump htop output
	echo q |htop |~/aha --black --line-fix >$outdir/htop.htm
}


function findlargedir {
	if [ $debug -gt 0 ] ; then 
		echo "Finding large files in /opt/icsranger"
	fi	
	find /opt/icsranger/ -type d -exec du -sh {} \;|sort -gr -k1 |head -10 > $outdir/biggest_10_opt_icsranger
	if [ $debug -gt 0 ] ; then 
		echo "Finding large files in /root"
	fi
	find /root/ -type d -exec du -sh {} \;|sort -gr -k1|head -10  > $outdir/biggest_10_root_home
	if [ $debug -gt 0 ] ; then 
		echo "Finding large files in /home"
	fi
	find /home/ -type d -exec du -sh {} \;|sort -gr -k1 |head -10 >> $outdir/biggest_10_root_home
	if [ $debug -gt 0 ] ; then 
		echo "Finding large files in /tmp"
	fi
	find /tmp/ -type d -exec du -sh {} \;|sort -gr |head -10  > $outdir/biggest_10_tmp
	if  [ $debug -gt 0 ] ; then 
		echo "Finding large files in /var/lib"
	fi
	find /var/lib/ -type d -exec du -sh {} \;|sort -gr |head -10 > $outdir/biggest_10_var_lib
	if [ $debug -gt 0 ] ; then 
		echo "Finding large files in  /var/tmp"
	fi
	find /var/tmp/ -type d -exec du -sh {} \;|sort -gr |head -10  > $outdir/biggest_10_var_tmp
}

function dbtablesize {
		
# mysql table sizes
mysql -u root dibi -e "SELECT TABLE_SCHEMA AS 'Database',TABLE_NAME AS 'Table', ROUND(DATA_LENGTH / 1024 / 1024) AS 'data_size (MB)', ROUND(INDEX_LENGTH / 1024 / 1024) AS 'index_size (MB)', ROUND((DATA_LENGTH + INDEX_LENGTH) / 1024 / 1024) AS 'total_size (MB)' FROM information_schema.TABLES where TABLE_SCHEMA='dibi' ORDER BY (DATA_LENGTH + INDEX_LENGTH) DESC  limit 20;" > $outdir/mysql_table_sizes
# dump mysql db
mysqldump -u root --add-drop-database -B dibi > $outdir/mysqldump_-uroot_--add-drop-database_-B_dibi
#postgres table sizes
psql -U postgres dibi -c "SELECT relname AS "relation",pg_size_pretty (pg_total_relation_size (C .oid)) AS "total_size" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C .relnamespace) WHERE nspname NOT IN ('pg_catalog','information_schema')AND C .relkind <> 'i' AND nspname !~ '^pg_toast' ORDER BY pg_total_relation_size (C .oid) DESC LIMIT 20;" > $outdir/postgres_table_sizes
# dump postgres db
pg_dump -U lkpo dibi --clean --create > $outdir/pg_dump_dibi_-U_lkpo_--clean_--create
}

function getfolders {
	mkdir $outdir/opt
	mkdir $outdir/opt/icsranger
	mkdir $outdir/opt/icsranger/workdir
	mkdir $outdir/etc
	mkdir $outdir/etc/icsranger
	mkdir $outdir/var
	mkdir $outdir/var/log
	mkdir $outdir/var/claroty
	mkdir $outdir/var/lib
	mkdir $outdir/var/spool
	ctdver=$(find /var/claroty/installers -name install.log|cut -d'/' -f5)
	 mkdir $outdir/var/claroty/installers
	 mkdir $outdir/var/claroty/installers/${ctdver}
 	 mkdir $outdir/var/claroty/installer
	 cp -r /opt/icsranger/lkpo* $outdir/opt/icsranger
	 cp -r /opt/icsranger/workdir/workers/authentication $outdir/opt/icsranger/workdir
	 cp -r /opt/icsranger/workdir/manager.db $outdir/opt/icsranger
	 cp -r /opt/icsranger/workdir/logs $outdir/opt/icsranger/
	 cp -r /opt/claroty $outdir/opt/
	 # /etc directory
	 etcdir=("cron.d" "logrotate.conf" "os-release" "yara")
	 for myfiles in ${etcdir[@]} ; do 
	 	cp -r /etc/$myfiles $outdir/etc
	 done	 
	 # /var/claroty 
	 cp -r /var/claroty/installer/logs $outdir/var/claroty/installer
	 cp -r /var/claroty/installers/ctd-*/install.log $outdir/var/claroty/installers/${ctdver}
	 # /var/lib
	 varlib=( "icsranger" "jwthenticator" "logrotate" "nettunnel" "redis")
	 for myfiles in ${varlib[@]} ; do 
	 	cp -r /var/lib/$myfiles $outdir/var/lib
	 done
	# /var/log
	varlog=("audit" "cron" "messages*" "nginx" "rabbitmq" "sa" "secure*" )
	for myfiles in ${varlog[@]} ; do 
	 	cp -r /var/log/$myfiles $outdir/var/log
	 done
	# /var/spool
	varspool=("cron")
	 for myfiles in ${varspool[@]} ; do 
	 	cp -r /var/spool/$myfiles $outdir/var/spool
	 done
	 # healthcheck output
	 /opt/icsranger/health_check.sh > $outdir/etc/icsranger/health_check_sh.txt
}

function procinfo {
	mkdir $outdir/proc
	cat /proc/cpuinfo > $outdir/proc/cpuinfo
	cat /proc/mounts > $outdir/proc/mounts
}

function runmenu {
	if [ -d /opt/icsranger/venv/lib/python3.9/site-packages/lkpo/admin_scripts/menu ] ; then 
			cd /opt/icsranger/venv/lib/python3.9/site-packages/lkpo/admin_scripts/menu
			python3 menu.pyc run json > $outdir/menu.py_run_json
	elif [ -d /opt/icsranger/venv/lib/python3.6/site-packages/lkpo/admin_scripts/menu ] ; then 
			cd /opt/icsranger/venv/lib/python3.6/site-packages/lkpo/admin_scripts/menu
			python3 menu.pyc run json > $outdir/menu.py_run_json
	else
			echo "No menu.py" > $outdir/menu.py_run_json
	fi
}

function compressfiles {
	cd /tmp
	XZ_OPT='-2' ; tar -cJf /tmp/${outfile} ${fname}
	cd /tmp
	# encrypt file 
	# 4.6 and above
	if [ -f /opt/icsranger/venv/lib/python3.9/site-packages/lkpo/logdump/team8_dump_key.pub ] ; then 
		pubkeyloc=/opt/icsranger/venv/lib/python3.9/site-packages/lkpo/logdump/team8_dump_key.pub
	else
		pubkeyloc=/opt/icsranger/venv/lib64/python3.6/site-packages/lkpo/logdump/team8_dump_key.pub
	fi
	gpg --no-default-keyring --trust-model always --keyring ${pubkeyloc} -r "Team8 Industrial Dump Key" --encrypt  --output ${outfile}.gpg ${outfile}
	if [ -d /opt/claroty-platform/sftp ] ;then 
		mv /tmp/${outfile}.gpg /opt/claroty-platform/sftp/home
		echo "Outfile is at /opt/claroty-platform/sftp/home/${outfile}.gpg"
	else
		echo "Outfile is at /tmp/${outfile}.gpg"
	fi
	rm -rf $outdir
	rm -rf ${outfile}
}

# Main function 

if [ ! -d $outdir ]; then 
	mkdir $outdir
fi

echo "Collecting stats and background info"
procinfo
echo "Getting device realtime information"
runcommands
echo "Getting DB info"
dbtablesize
echo "Running menu.sh"
runmenu
echo "Finding large files in filesystem"
findlargedir
echo "Fetching logs"
getfolders
echo "Compressing and encrypting file"
compressfiles
